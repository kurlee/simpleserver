import socket
from optparse import OptionParser


class HTTPclient(socket.socket):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        try:
            self.client = socket.create_connection((self.host, self.port))
            print("Socket Object created")
        except Exception as e:
            print("failed ::" + str(e))

    def GET(self, request):
        message= 'GET /' + request + ' HTTP/1.1\r\nHost: ' + str(self.host) + ':'\
        + str(self.port) + '\r\nUser-Agent: AHA392 Python 3 Client\r\n\r\n'
        self.client.sendall(message.encode())
        responsePart = self.client.recv(4096)
        response = ''
        while (len(responsePart) > 0):
            response += responsePart.decode()
            responsePart = self.client.recv(4096)
        #responseHeader = self.client.recv(4096)
        #responseBody = self.client.recv(4096)
        return (response)


def main():
    usage = "usage: %prog [options] -H <host ip> -p <port> -f <filename>"
    parser = OptionParser(usage = usage)
    parser.add_option("-H", dest="host",type = 'string', help="Specify host")
    parser.add_option("-p", dest="port",type="int", help="Specify port")
    parser.add_option("-f", dest="filename", type="string", help="Specify filename")
    (options, args) = parser.parse_args()
    host = options.host
    port = options.port
    filename = options.filename

    client = HTTPclient(host, port)
    print(client.GET(filename))




if __name__ == "__main__":
    print('starting')
    main()
