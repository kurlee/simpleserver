# Simple Server program written in Python 3.7 for NYU class Computer Networking
# Skeleton code provided by Computer Networking: A Top-Down Approach
# Every thing else by Adrian Abdala
# https://docs.python.org/3/library/socket.html
# https://docs.python.org/3/howto/sockets.html


# !/usr/bin/env python3

import threading
import datetime
import socket
import sys


def client_thread(connectionSocket):
    try:
        # Receive request from client and parse information requested 
        message = connectionSocket.recv(4096)
        filename = (message.decode()).split()[1]
        outputdata = open(filename[1:]).read()
        # Send one HTTP header line into socket
        # Fill in start
        HTTPheader = 'HTTP/1.1 200 OK\nContent-Type: text/html\n\n'
        connectionSocket.send((HTTPheader).encode())
        # log information about connection
        log(str(connectionSocket.getpeername()[0]), (message.decode()).splitlines(), HTTPheader) 
        # Fill in end
        # Send the content of the requested file to the client
        connectionSocket.sendall(outputdata.encode())
        connectionSocket.close()
    except IOError as e:
        # Send response message for file not found
        # Fill in start
        print('Requested file not found: ' +str(e))
        HTTPheader = 'HTTP/1.1 404 Not found\nContent-Type: text/html\n\n'
        connectionSocket.send(HTTPheader.encode())
        #log information about connection
        log(str(connectionSocket.getpeername()[0]), message.decode(), HTTPheader) 
        connectionSocket.send(('<h1>404 Not Found</h1>').encode())
        # Fill in end
        # Close client socket
        # Fill in start
        connectionSocket.close()
        # Fill in e


def log(host, message, HTTPheader):
        log = open('log.txt', 'a')
        log.write(host + '\n' + str(datetime.datetime.now()) + '\n')
        log.write(message[0] + '\n')
        log.write(HTTPheader.split('\n', 1)[0])
        for line in message:
            if 'User-Agent' in line:
                log.write('\n' + line)
        log.write('\n' + 25 * '*' + '\n')



def main():
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port = 8081

    # Prepare server socket and throw exception if there are any issues.
    # Possible issues could be port alredy being used for service
    try:
        serverSocket.bind(('', port))
        serverSocket.listen(5)
    except socket.error as msg:
        print("Error occured in server socket preparation:" + str(msg))
        sys.exit(2)

    # Display hostname and port
    socketAddrInfo = serverSocket.getsockname()
    print("Server setup complete...\n"
        "Host Address: " + str(socketAddrInfo[0]) + '\n'
        "Port        : " + str(socketAddrInfo[1])
        )

    print('\n\nReady to serve...')
    # Prepare a sever socket
    # Fill in start
    while True:
        (connectionSocket, addr) = serverSocket.accept()
        ct = threading.Thread(target=client_thread, args=(connectionSocket, ))
        ct.start()


if __name__ == "__main__":
    main()
